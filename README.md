<h1>Usluga za rangiranje i preporuku sadržaja na temelju društvenih podataka</h1>
<h3>Josip Hranić, Mislav Gillinger, Vilim Prodanović, Matej Jelušić</h3>

<strong>Društveni podaci</strong> su podaci nastali kao aktivnost korisnika na različitim informacijsko komunikacijskim
platformama. Društveni podaci za pojedini tip sadržaja mogu biti u vidu dodijeljene subjektivne ocjene
korisnika, napisanog vlastitog doživljaja i mišljenja korisnika u vidu kraćeg komentara ili dužeg teksta
na specijaliziranim platformama. Također, društveni podaci su interakcije korisnika na različitim
društvenim mrežama u vidu oznaka da mu se sadržaj sviđa (npr. lajkova), dijeljenja sadržaja,
dodjeljivanje hashtagova, komentiranja, praćenja službenih stranica, broj pregleda pojedinog sadržaja.

<strong>Zadatak</strong> je prikupiti što više različitih društvenih tipova podataka sa što više različitih izvora u svrhu
razvoja liste popularnih filmova i glazbe te personalizirane preporuke sadržaja za korisnike.

Usluga koju je potrebno razviti sastoji se od:
1. sustava za kreiranje profila sadržaja tj. baze sadržaja (filmova, glazbe);
2. sustava za izradu liste popularnosti sadržaja;
3. sustava za personaliziranu preporuku sadržaja za svakog korisnika usluge.

<strong>Prvi dio</strong> „sustava za kreiranje profila sadržaja“ ima za <strong>cilj kreiranje baze sadržaja</strong>. Baza sadržaja se
sastoji od <strong>dva</strong> glavna dijela, a to su:
<ul>
<li>opći podaci o sadržaju;</li>
<li>društveni podaci o sadržaju</li>
</ul>

<table>
<tr>
    <td><strong>Primjer općih podataka za filmove</strong></td>
    <td><strong>Primjer društvenih podataka za filmove</strong></td>
</tr>

</td>
<tr>
<td>
• ime filma

• žanr filma

• godina izdanja filma

• nagrada filma (Oscar, ...)

• ime glumca

• zarada filma

• trailer filma

• …
</td>
<td>
• ocjene filma na specijaliziranim web
stranicama

    - ocjena stručnjaka
    - ocjena korisnika
• trailer filma

    - broj pregleda trailer filma
    - broj lajkova trailera
    - broj dijeljenja trailera
• broj fanova filma na društvenim mrežama

• analiza komentara i recenzija filmova

• …</td>
</tr>
</table>




<strong>Drugi dio</strong> „sustava za izradu liste popularnosti sadržaja“ podrazumijeva izradu liste popularnog
sadržaja na temelju društvenih podataka za sve korisnike jednako. Lista ima vremensku komponentu
koja se sačinjava od npr. liste najpopularnijih filmova na mjesečnoj, godišnjoj razini i sl. te može
sadržavati mogućnosti pregleda po kategorijama sadržaja (npr. žanrovi filma).
FER – ZAVOD ZA TELEKOMUNIKACIJE Ak. god. 2018./2019.
3

<strong>Treći dio</strong> „sustava za personaliziranu preporuku sadržaja za svakog korisnika usluge“ sastoji se od
omogućavanja prijave u sustav koristeći društvene mreže (npr. Facebook, Twitter, Google). Na temelju
preferencija korisnika sustava korisniku se prikazuje personalizirana preporuka sadržaja. Preferencije
korisnika mogu biti detektirane indirektno i eksplicitno. Indirektna detekcija podrazumijeva dohvat
podataka iz profila korisnika na društvenoj mreži Facebook npr. na način da korisnik označi određene
filmove koji mu se sviđaju ili je fan službenog profila Facebook stranice određenog filma, glumca i sl.
Eksplicitne preferencije podrazumijevanju nakon prvog ulaska u aplikaciju da se korisniku ponudi izbor
da odabere npr. određene žanrove filma koje voli.

Nadalje, korisnicima se mogu prikazivati najnovije vijesti i događaji za definirane sadržaj npr. vijesti
New York Times, Twitter hashtagovi, također korisnicima usluge se mogu preporučivati vrijeme
gledanja filmova na temelju vremenske prognoze npr. gledanje filmova za vrijeme kiše.
Način preporuke tj. metoda preporuke sadržaja nije unaprijed zadana te je prepuštena na
odabir/kreativnost studentskih timova. Preporučitelj se može sastojati od različitih opcija za preporuke
koje obuhvaćaju:

• slične korisnike;

• slične predmeta;

• praćenje korisničke interakcije;

• popularnih predmeta;

• vremenska komponenta interakcije s predmetom.

Cilj preporučitelja je uključiti različite opcije društvenih podataka i interakcija korisnika kako bi isti
dobili što bolju listu preporuka za navedene filmove i time dobili personaliziranu listu. Nastavno na
takvu interakciju najbolji radovi trebaju moći objasniti interni algoritam rada sustava za preporuke kao
i korištene interakcije.
Neki od prijedloga za korištenje izvora podataka:

    facebook.com
    twitter.com
    instagram
    messenger
    omdbapi.com
    themoviedb.org
    trakt.tv
    developers.google.com
    last.fm
    tvmaze.com
    developer.nytimes.com

Usluge vremenske prognoze:
  
    openweathermap.org/api
    developer.yahoo.com/weather/

Lokacijske usluge:
    
    developer.here.com
    bingmapsportal.com

Web stranica s popisom velikog broja API-ja po različitim kategorijama:

    programmableweb.com

