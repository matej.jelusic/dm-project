package fer.dm.projekt.security;

public class LoginDetails {

    String token;

    public LoginDetails() {
    }

    public LoginDetails(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
