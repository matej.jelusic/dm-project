package fer.dm.projekt.repository;

import fer.dm.projekt.model.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {

    public UserModel findByFirstName(String firstName);
    public List<UserModel> findByLastName(String lastName);
    public boolean existsUserModelByEmail(String email);
    public UserModel findByFacebookKey(String key);

}