package fer.dm.projekt;

import fer.dm.projekt.service.ImportService;
import fer.dm.projekt.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.*;

@SpringBootApplication
public class Application {


    @Autowired
    public ImportService importService;

    @Autowired
    public MovieService movieServiceImpl;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }



    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() throws InterruptedException, IOException {
        //skrozNoviImportService.importMoviesToDatabase();
        importService.importMoviesFromJsonFile();
        //System.out.println(movieServiceImpl.getLastTweetsforMovieTitle("Delivery Man Movie"));
        //MovieModel movieModel = movieServiceImpl.getMostPopularMovies().get(14);
//        movieServiceImpl.getMovieDetailsFromOMDB(movieModel.getTitle());
//        movieServiceImpl.getSimilarMoviesFromTasteDive(movieModel.getTitle());
//        movieServiceImpl.getMovieDetailsFromOMDB(movieModel.getTitle());

        //movieServiceImpl.getMovieByTitle("aquaman");

        //movieServiceImpl.getLastTweetsforMovieTitle("aquaman");


//        InputStream is = Application.class.getResourceAsStream("./tmdbMovieModel.json");
        //

        //System.out.println(movieServiceImpl.getMoviesForInitChoosingForUser("2437646796262714"));
        System.out.println("hello world, I have just started up");
    }
}
