package fer.dm.projekt.model;

public class SimpleMovieDto {
    private String id;
    private String title;
    private String year;
    private String image;

    public SimpleMovieDto(String id, String title, String year, String image) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public SimpleMovieDto() {
    }

    public SimpleMovieDto(TmdbMovieModel movie) {
        this.id = movie.getId();
        this.title = movie.getTitle();
        if(movie.getReleaseDate() != null){
            this.year = movie.getReleaseDate().substring(0, 4);
        }
        this.image = TmdbMovieModel.TMDB_IMAGE_API_ENDPOINT + "" + movie.getPosterPath();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
