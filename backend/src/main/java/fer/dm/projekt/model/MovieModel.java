package fer.dm.projekt.model;

import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class MovieModel implements Serializable {
    @Id
    String id;
    String title;
    String year;
    String runtime;
    String director;
    List<String> actors;
    String plot;
    String language;
    boolean adult;
    double tmdbPopularity;

    String tmdbId;
    String imdbId;

    double tmdbRating;
    long tmdbVotes;

    double imdbRating;
    long imdbVotes;

    String rottenTomatoesRating;
    String metacriticRating;

    List<String> genres;
    String posterPath;
    String coverPath;
    String youtubeLink;

    long currentWatchCount;
    long movieMentorRating;

    public MovieModel() {
    }


    public MovieModel(String title, String year, String runtime, String director, List<String> actors, String plot, String language, boolean adult, double tmdbPopularity, String tmdbId, String imdbId, double tmdbRating, long tmdbVotes, double imdbRating, long imdbVotes, String rottenTomatoesRating, String metacriticRating, List<String> genres, String posterPath, String coverPath, String youtubeLink, long currentWatchCount) {
        this.title = title;
        this.year = year;
        this.runtime = runtime;
        this.director = director;
        this.actors = actors;
        this.plot = plot;
        this.language = language;
        this.adult = adult;
        this.tmdbPopularity = tmdbPopularity;
        this.tmdbId = tmdbId;
        this.imdbId = imdbId;
        this.tmdbRating = tmdbRating;
        this.tmdbVotes = tmdbVotes;
        this.imdbRating = imdbRating;
        this.imdbVotes = imdbVotes;
        this.rottenTomatoesRating = rottenTomatoesRating;
        this.metacriticRating = metacriticRating;
        this.genres = genres;
        this.posterPath = posterPath;
        this.coverPath = coverPath;
        this.youtubeLink = youtubeLink;
        this.currentWatchCount = currentWatchCount;
    }

    public MovieModel(OmdbMovieModel omdbMovieModel, TmdbMovieModel tmdbMovieModel, long currentWatchCount, long movieMentorRating) {
        if(omdbMovieModel != null){
            this.title = omdbMovieModel.getTitle();
            this.year = omdbMovieModel.getYear();
            this.runtime = omdbMovieModel.getRuntime();
            this.director = omdbMovieModel.getDirector();
            this.actors = getListFromString(omdbMovieModel.getActors());
            this.plot = omdbMovieModel.getPlot();
            this.language = omdbMovieModel.getLanguage();
            this.imdbId = omdbMovieModel.getImdbID();
            try{
                this.imdbRating = Double.parseDouble(omdbMovieModel.getImdbRating());
            } catch (Exception e){
                System.out.println("Error parsing imdb rating for movie: " + omdbMovieModel.getTitle());
            }
            this.rottenTomatoesRating = getRating("Rotten Tomatoes", omdbMovieModel.getRatings());
            this.metacriticRating = getRating("Metacritic", omdbMovieModel.getRatings());
            this.genres = getListFromString(omdbMovieModel.getGenre());
            this.posterPath = omdbMovieModel.getPoster();
            if (omdbMovieModel.getImdbVotes() != null) {
                try{
                    this.imdbVotes = Long.parseLong(omdbMovieModel.getImdbVotes().replaceAll("[,]", ""));
                } catch (Exception e) {
                    System.out.println("Error parsing imdb votes for movie " + omdbMovieModel.getTitle());
                }
            }
        }

        if(tmdbMovieModel != null){
            this.id = tmdbMovieModel.getId();
            this.adult = tmdbMovieModel.isAdult();
            this.tmdbPopularity = tmdbMovieModel.getPopularity();
            this.tmdbId = tmdbMovieModel.getId();
            this.tmdbRating = tmdbMovieModel.getVoteAverage();
            this.tmdbVotes = tmdbMovieModel.getVoteCount();
            this.coverPath = TmdbMovieModel.TMDB_IMAGE_API_ENDPOINT + "" + tmdbMovieModel.getBackdropPath();
            this.youtubeLink = tmdbMovieModel.getYoutubeLink();
        }

        this.currentWatchCount = currentWatchCount;
        this.movieMentorRating = movieMentorRating;
    }

    private String getRating(String site, List<Rating> ratings) {
        if(ratings != null){
            for (Rating rating : ratings) {
                if (rating.getSource().equals(site)) {
                    return rating.getValue();
                }
            }
        }
        return null;
    }

    private List<String> getListFromString(String actors) {
        if (actors != null) {
            String[] actorsStrings = actors.trim().split(",");
            List<String> actorList = new LinkedList<>();
            for (String actorString : actorsStrings) {
                actorList.add(actorString.trim());
            }
            return actorList;
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public double getTmdbPopularity() {
        return tmdbPopularity;
    }

    public void setTmdbPopularity(double tmdbPopularity) {
        this.tmdbPopularity = tmdbPopularity;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public double getTmdbRating() {
        return tmdbRating;
    }

    public void setTmdbRating(double tmdbRating) {
        this.tmdbRating = tmdbRating;
    }

    public long getTmdbVotes() {
        return tmdbVotes;
    }

    public void setTmdbVotes(long tmdbVotes) {
        this.tmdbVotes = tmdbVotes;
    }

    public double getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(double imdbRating) {
        this.imdbRating = imdbRating;
    }

    public long getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(long imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public String getRottenTomatoesRating() {
        return rottenTomatoesRating;
    }

    public void setRottenTomatoesRating(String rottenTomatoesRating) {
        this.rottenTomatoesRating = rottenTomatoesRating;
    }

    public String getMetacriticRating() {
        return metacriticRating;
    }

    public void setMetacriticRating(String metacriticRating) {
        this.metacriticRating = metacriticRating;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public long getCurrentWatchCount() {
        return currentWatchCount;
    }

    public void setCurrentWatchCount(long currentWatchCount) {
        this.currentWatchCount = currentWatchCount;
    }

    public long getMovieMentorRating() { return movieMentorRating; }

    public void setMovieMentorRating(long movieMentorRating) { this.movieMentorRating = movieMentorRating; }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieModel that = (MovieModel) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(year, that.year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, year);
    }

    @Override
    public String toString() {
        return "MovieModel{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", runtime='" + runtime + '\'' +
                ", director='" + director + '\'' +
                ", actors='" + actors + '\'' +
                ", plot='" + plot + '\'' +
                ", language='" + language + '\'' +
                ", adult=" + adult +
                ", tmdbPopularity='" + tmdbPopularity + '\'' +
                ", tmdbId='" + tmdbId + '\'' +
                ", imdbId='" + imdbId + '\'' +
                ", tmdbRating='" + tmdbRating + '\'' +
                ", tmdbVotes='" + tmdbVotes + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", imdbVotes='" + imdbVotes + '\'' +
                ", rottenTomatoesRating='" + rottenTomatoesRating + '\'' +
                ", metacriticRating='" + metacriticRating + '\'' +
                ", genres=" + genres +
                ", posterPath='" + posterPath + '\'' +
                ", coverPath='" + coverPath + '\'' +
                ", youtubeLink='" + youtubeLink + '\'' +
                '}';
    }
}
