package fer.dm.projekt.model;

public class TweetModel {
    String createdAt;
    String text;
    String user;

    public TweetModel() {
    }

    public TweetModel(String createdAt, String text, String user) {
        this.createdAt = createdAt;
        this.text = text;
        this.user = user;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
