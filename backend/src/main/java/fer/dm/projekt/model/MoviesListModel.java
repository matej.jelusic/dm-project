package fer.dm.projekt.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MoviesListModel {
    @JsonProperty("page")
    private int page;

    @JsonProperty("total_pages")
    private int totalPages;
    private List<TmdbMovieModel> results;

    public MoviesListModel() {
    }

    public MoviesListModel(List<TmdbMovieModel> results) {
        this.results = results;
    }

    public List<TmdbMovieModel> getResults() {
        return results;
    }

    public void setResults(List<TmdbMovieModel> results) {
        this.results = results;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
