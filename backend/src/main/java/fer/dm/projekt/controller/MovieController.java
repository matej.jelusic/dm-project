package fer.dm.projekt.controller;


import fer.dm.projekt.model.MovieModel;
import fer.dm.projekt.model.SimpleMovieDto;
import fer.dm.projekt.model.TweetModel;
import fer.dm.projekt.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class MovieController {

    @Autowired
    MovieService movieService;




    @GetMapping("/api/home")
    public void sayHello(){
        System.out.println("HELLO");
    }


    @GetMapping("/api/movie/title/{id}")
    public MovieModel getMovieByTitle(@PathVariable String id){
        return movieService.getMovieByTitle(id);
    }

    @GetMapping("/api/movie/popularity")
    public List<MovieModel> getMostPopular(){
        return movieService.getMostPopularMovies();
    }

    @GetMapping("/api/movie/recommendations/{userId}")
    public List<MovieModel> getRecommendationForUser(@PathVariable String userId){
        return movieService.getRecommendedMoviesForUser(userId);
    }

    @GetMapping("/api/movie/init/choosing/{userId}")
    public List<MovieModel> getMoviesForInitChoosing(@PathVariable String userId){
        return movieService.getMoviesForInitChoosingForUser(userId);
    }

    @PostMapping("/api/movie/init/choosing/{userId}")
    public void addInitialChoosenMovies(@PathVariable String userId, @RequestBody List<MovieModel> movieModelList){
        movieService.addInitialChoosenMovies(userId, movieModelList);
    }

    @GetMapping("/api/tweets/{movieId}")
    public List<TweetModel> getTweetsForMovie(@PathVariable String movieId){
        return movieService.getLastTweetsforMovieId(movieId);
    }

    @GetMapping("/api/favourites/{userId}")
    public Set<MovieModel> getFavouritesForUser(@PathVariable String userId){
        return movieService.getFavourites(userId);
    }

    @PostMapping("/api/favourites/{userId}")
    public Set<MovieModel> addToFavouritesForUser(@PathVariable String userId, @RequestBody MovieModel movieModel){
        return movieService.addToFavourites(userId, movieModel);
    }

    @DeleteMapping("/api/favourites/{userId}")
    public Set<MovieModel> deleteFromFavouritesForUser(@PathVariable String userId, @RequestBody MovieModel movieModel){
        return movieService.removeFromFavourites(userId, movieModel);
    }

    @GetMapping("/api/movies/autocomplete")
    public List<SimpleMovieDto> getAutocompleteMovies(@RequestParam String query){
        return movieService.getAutocompleteMovies(query);
    }

    @PostMapping("/api/movie/init/choosing/new/{userId}")
    public void addNewInitialChoosenMovies(@PathVariable String userId, @RequestBody List<SimpleMovieDto> movieModelList){
        movieService.addNewInitialChoosenMovies(userId, movieModelList);
    }
}
