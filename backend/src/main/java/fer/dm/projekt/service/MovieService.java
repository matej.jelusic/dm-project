package fer.dm.projekt.service;



import fer.dm.projekt.model.OmdbMovieModel;
import fer.dm.projekt.model.MovieModel;
import fer.dm.projekt.model.SimpleMovieDto;
import fer.dm.projekt.model.TweetModel;

import java.util.List;
import java.util.Set;

public interface MovieService {
    public List<MovieModel> getMostPopularMovies();
    public List<MovieModel> getRecommendedMoviesForUser(String userId);
    MovieModel getMovieByTitle(String title);
    List<MovieModel> getMoviesForInitChoosingForUser(String userId);
    void addNewInitialChoosenMovies(String userId, List<SimpleMovieDto> movieModelList);
    void addInitialChoosenMovies(String userId, List<MovieModel> movieModelList);
    OmdbMovieModel getMovieDetailsFromOMDB(String title);
    public List<String> getSimilarMoviesFromTasteDive(String title);
    public List<String> getSimilarMoviesFromTmdb(String title);
    public List<TweetModel> getLastTweetsforMovieId(String movieId);
    public List<MovieModel> getMoviesByDirector(String actorName);
    public List<String> getMoviesByActor(String actorName);
    public Set<MovieModel> addToFavourites(String userId, MovieModel movieModel);
    public Set<MovieModel> removeFromFavourites(String userId, MovieModel movieModel);
    public Set<MovieModel> getFavourites(String userId);
    public List<SimpleMovieDto> getAutocompleteMovies(String query);

}
