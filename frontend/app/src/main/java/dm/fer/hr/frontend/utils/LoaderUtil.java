package dm.fer.hr.frontend.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import dm.fer.hr.frontend.Constants;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.UserModel;

public class LoaderUtil {

    public LoaderUtil() {

    }

    public void loadPosterImageFromUrl(MovieModel movie, SimpleCallback<Bitmap> callback) {
        loadImageFromUrl(movie.getPosterPath(), callback);
    }

    public void loadCoverImageFromUrl(MovieModel movie, SimpleCallback<Bitmap> callback) {
        loadImageFromUrl(movie.getCoverPath(), callback);
    }

    private void loadImageFromUrl(String url, SimpleCallback<Bitmap> callback) {
        List<Bitmap> results = new ArrayList<>();
        LinkedBlockingQueue<Runnable> tasks = new LinkedBlockingQueue<Runnable>();
        tasks.add(new Runnable() {
            @Override
            public void run() {
                Bitmap result = loadImageFromUrl(url);
                Log.d("LOADIMAGEFROMURL", "loaded icon for movie");
                results.add(result);
            }
        });
        ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 6, 2, TimeUnit.SECONDS, tasks);
        executor.prestartAllCoreThreads();

        Handler handler = new Handler();
        Runnable runnableChecker = new Runnable() {
            @Override
            public void run() {
                Log.d("LOADIMAGEFROMURL", "completed:" + executor.getCompletedTaskCount() + "/" + executor.getTaskCount()
                        + " active:" + executor.getActiveCount());
                if (executor.getCompletedTaskCount() == executor.getTaskCount()) {
                    callback.onSuccess(results.get(0), 0, null);
                    executor.shutdown();
                    Log.d("LOADIMAGEFROMURL", "shutdown");
                } else {
                    handler.postDelayed(this, 500);
                }
            }
        };
        handler.post(runnableChecker);
    }

    public void loadPosterImagesFromUrls(List<MovieModel> movies, SimpleCallback<HashMap<String, Bitmap>> callback) {
        List<String> ids = getMoviesIds(movies);
        List<String> urls = movies.stream()
                .map(MovieModel::getPosterPath)
                .collect(Collectors.toList());

        loadImagesFromUrls(ids, urls, callback);
    }


    public void loadCoverImagesFromUrls(List<MovieModel> movies, SimpleCallback<HashMap<String, Bitmap>> callback) {
        List<String> ids = getMoviesIds(movies);
        List<String> urls = movies.stream()
                .map(MovieModel::getCoverPath)
                .collect(Collectors.toList());

        loadImagesFromUrls(ids, urls, callback);
    }

    private void loadImagesFromUrls(List<String> ids, List<String> urls, SimpleCallback<HashMap<String, Bitmap>> callback) {
        HashMap<String, Bitmap> result = new HashMap<>();
        LinkedBlockingQueue<Runnable> tasks = new LinkedBlockingQueue<Runnable>();
        for(int i = 0; i < ids.size(); i++) {
            final int pos = i;
            tasks.add(new Runnable() {
                @Override
                public void run() {
                    Bitmap movieIcon = loadImageFromUrl(urls.get(pos));
                    Log.d("LOADIMAGEFROMURL", "loaded icon for movieID:" + ids.get(pos));
                    result.put(ids.get(pos), movieIcon);
                }
            });
        }

        ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 6, 2, TimeUnit.SECONDS, tasks);
        executor.prestartAllCoreThreads();

        //periodic check of loaded image
        Handler handler = new Handler();
        Runnable runnableChecker = new Runnable() {
            @Override
            public void run() {
                Log.d("LOADIMAGEFROMURL", "completed:" + executor.getCompletedTaskCount() + "/" + executor.getTaskCount()
                        + " active:" + executor.getActiveCount());
                if (executor.getCompletedTaskCount() != executor.getTaskCount()) {
                    callback.onSuccess(result, 0, null);
                    handler.postDelayed(this, 500);
                } else {
                    callback.onSuccess(result, 0, null);
                    executor.shutdown();
                    Log.d("LOADIMAGEFROMURL", "shutdown");
                }
            }
        };
        handler.post(runnableChecker);
    }

    private Bitmap loadImageFromUrl(String imageUrl) {
        URL url;
        Bitmap bmp = null;
        try {
            url = new URL(imageUrl);
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
    }

    private List<String> getMoviesIds(List<MovieModel> movies) {
        return movies.stream()
                .map(m -> m.getTmdbId() + m.getId())
                .collect(Collectors.toList());

    }
}
