package dm.fer.hr.frontend.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.MainActivity;
import dm.fer.hr.frontend.OnItemSelectedListener;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.adapters.SimpleMovieAdapter;
import dm.fer.hr.frontend.utils.SharedObjects;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.model.SimpleMovieDto;
import io.swagger.client.model.UserModel;

public class AccountFragment extends Fragment implements SearchView.OnQueryTextListener, OnItemSelectedListener<SimpleMovieDto> {

    private RecyclerView searchRecyclerView;
    private RecyclerView pickedRecyclerView;
    private RelativeLayout pickedRelativeLayout;
    private FloatingActionButton sendFloatingActionButton;
    private TextView numTextView;
    private Set<SimpleMovieDto> movies;
    private UserModel user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {
        SharedObjects<UserModel> sharedObjects = new SharedObjects<UserModel>(getActivity());
        user = sharedObjects.retrive(Constants.user, UserModel.class);

        ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.user_ppv);
        TextView userNameTextView = (TextView) view.findViewById(R.id.user_name_tv);
        TextView emailTextView = (TextView) view.findViewById(R.id.email_tv);

        profilePictureView.setProfileId(user.getId());
        userNameTextView.setText(user.getFirstName() + " " + user.getLastName());
        emailTextView.setText(user.getEmail());

        initSearch(view);
    }

    private void initSearch(View view) {
        searchRecyclerView = view.findViewById(R.id.search_rv);
        pickedRelativeLayout = view.findViewById(R.id.picked_layout);
        pickedRecyclerView = view.findViewById(R.id.picked_rv);
        sendFloatingActionButton = view.findViewById(R.id.send_fab);
        numTextView = view.findViewById(R.id.num_tv);
        SearchView searchView = view.findViewById(R.id.movie_sv);

        searchRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pickedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        searchView.setOnQueryTextListener(this);
        movies = new HashSet<>();
        numTextView.setText(movies.size() + "/" + 5);
        sendFloatingActionButton.setEnabled(false);

        sendFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMovies();
            }
        });
    }

    private void sendMovies() {
        MovieApi movieApi = new MovieApi();

        try {
            movieApi.apiMovieInitChoosingNewUserIdPostAsync(user.getId(), new ArrayList<>(movies), new SimpleCallback<Void>(getChildFragmentManager()) {
                @Override
                public void onSuccess(Void result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        movies = new HashSet<>();
                        numTextView.setText(movies.size() + "/" + 5);
                        sendFloatingActionButton.setEnabled(false);
                        SimpleMovieAdapter adapter = new SimpleMovieAdapter(getContext(), new ArrayList<>(movies));
                        pickedRecyclerView.setAdapter(adapter);
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }

    }

    private void updateSearchRecycler( List<SimpleMovieDto> movies) {
        pickedRelativeLayout.setVisibility(View.GONE);
        searchRecyclerView.setVisibility(View.VISIBLE);
        SimpleMovieAdapter adapter = new SimpleMovieAdapter(getContext(), movies);
        adapter.setOnItemSelectedListener(this);
        searchRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        MovieApi movieApi = new MovieApi();
        try {
            movieApi.apiMoviesAutocompleteGetAsync(query, new SimpleCallback<List<SimpleMovieDto>>(getFragmentManager()) {
                @Override
                public void onSuccess(List<SimpleMovieDto> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    super.onSuccess(result, statusCode, responseHeaders);
                    getActivity().runOnUiThread(() -> {
                        updateSearchRecycler(result);
                    });
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.equals("")) {
            searchRecyclerView.setVisibility(View.GONE);
            pickedRelativeLayout.setVisibility(View.VISIBLE);
        }
        return false;
    }


    @Override
    public void onSelect(SimpleMovieDto item, boolean isSelected) {
        searchRecyclerView.setVisibility(View.GONE);
        pickedRelativeLayout.setVisibility(View.VISIBLE);

        movies.add(item);
        SimpleMovieAdapter adapter = new SimpleMovieAdapter(getContext(), new ArrayList<>(movies));
        pickedRecyclerView.setAdapter(adapter);

        int limit = 5;
        numTextView.setText(movies.size() + "/" + limit);
        sendFloatingActionButton.setEnabled(movies.size()>=limit);
    }
}
