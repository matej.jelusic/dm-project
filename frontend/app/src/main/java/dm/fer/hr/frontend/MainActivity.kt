package dm.fer.hr.frontend

import android.animation.ValueAnimator
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.animation.*
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import dm.fer.hr.frontend.utils.SharedObjects
import dm.fer.hr.frontend.utils.SimpleCallback
import io.swagger.client.ApiException
import io.swagger.client.api.UserApi
import io.swagger.client.model.Token
import io.swagger.client.model.UserModel
import java.util.*
import android.widget.ImageView
import dm.fer.hr.frontend.dialogs.PickMoviesDialog
import android.widget.TextView
import dm.fer.hr.frontend.utils.CustomSpringInterpolator
import dm.fer.hr.frontend.utils.DestroyListener

class MainActivity : AppCompatActivity() {

    private var callbackManager: CallbackManager? = null

    private var mLoginButton : LoginButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initLoginButton()
        startAnimations()
    }

    private fun initLoginButton() {
        callbackManager = CallbackManager.Factory.create()

        FacebookSdk.sdkInitialize(getApplicationContext())
        AppEventsLogger.activateApp(this)

        val loginButton = findViewById<LoginButton>(R.id.login_button)
        loginButton.setReadPermissions(Arrays.asList("user_status", "user_likes", "user_tagged_places", "user_videos", "public_profile"
                , "user_gender", "user_friends", "user_photos", "user_birthday", "user_hometown", "user_link", "user_posts", "user_location", "user_age_range", "email"))

        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                onLoginSuccess(loginResult)
            }

            override fun onCancel() {
                // App code TODO
            }

            override fun onError(exception: FacebookException) {
                // App code TODO
            }
        })

        mLoginButton = loginButton
    }

    fun onLoginSuccess(loginResult: LoginResult) {
        val accessToken = loginResult.accessToken.token
        val userApi = UserApi()
        var token = Token()
        token.token = accessToken
        userApi.logUserAsync(token, object : SimpleCallback<UserModel>(supportFragmentManager) {
            override fun onSuccess(result: UserModel?, statusCode: Int, responseHeaders: MutableMap<String, MutableList<String>>?) {
                val sharedObject = SharedObjects<UserModel>(applicationContext)
                sharedObject.put(Constants.user, result)

//                val sharedObjectBoolean = SharedObjects<Boolean>(applicationContext)
//                val choosenInitMovies = sharedObjectBoolean.retrive(Constants.pickedInitMovies, Boolean::class.java)
                if (result!!.initChosenMovies == null || result!!.initChosenMovies.isEmpty()) {
                    val pickMoviesDialog = PickMoviesDialog()
                    pickMoviesDialog.show(supportFragmentManager, "PickMoviesDialog")
                    pickMoviesDialog.setOnDestroyListenr {
                    }
                } else {
                    val intent = Intent(applicationContext, TabbedActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

            override fun onFailure(e: ApiException?, statusCode: Int, responseHeaders: MutableMap<String, MutableList<String>>?) {
                super.onFailure(e, statusCode, responseHeaders)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun startAnimations() {
        val rotateAnimation = RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotateAnimation.interpolator = LinearInterpolator()
        rotateAnimation.repeatCount = 2//Animation.INFINITE
        rotateAnimation.duration = 250

        val alphaAnimation = AlphaAnimation(0.0f, 1.0f)
        alphaAnimation.interpolator = LinearInterpolator()
        alphaAnimation.duration = 2000

        val scaleAnimation = ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.interpolator = CustomSpringInterpolator()
        scaleAnimation.duration = 2000

        val translateFromTopAnimation = TranslateAnimation(0f,0f,-1000f,0f)
        translateFromTopAnimation.interpolator = LinearInterpolator()
        translateFromTopAnimation.duration = 500

        val translateFromBottomAnimation = TranslateAnimation(0f,0f,1000f,0f)
        translateFromTopAnimation.interpolator = LinearInterpolator()
        translateFromTopAnimation.duration = 500

        val imageAnimationSet = AnimationSet(false)
        imageAnimationSet.addAnimation(rotateAnimation)
        imageAnimationSet.addAnimation(alphaAnimation)
        imageAnimationSet.addAnimation(scaleAnimation)
        imageAnimationSet.addAnimation(translateFromTopAnimation)

        val buttonAnimationSet = AnimationSet(false)
        buttonAnimationSet.addAnimation(alphaAnimation)
        buttonAnimationSet.addAnimation(scaleAnimation)
        buttonAnimationSet.addAnimation(translateFromBottomAnimation)

        val titleAnimationSet = AnimationSet(false)
        titleAnimationSet.addAnimation(alphaAnimation)
        titleAnimationSet.addAnimation(scaleAnimation)

        val imageView = findViewById<ImageView>(R.id.app_iv)
        val welcomeTextView = findViewById<TextView>(R.id.welcome_tv)

        imageView.startAnimation(imageAnimationSet)
        mLoginButton!!.startAnimation(buttonAnimationSet)
        welcomeTextView.startAnimation(titleAnimationSet)
    }

    private fun initImageView() {
        val rotateAnimation = RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotateAnimation.interpolator = LinearInterpolator()
        rotateAnimation.repeatCount = 2//Animation.INFINITE
        rotateAnimation.duration = 250

        val alphaAnimation = AlphaAnimation(0.0f, 1.0f)
        alphaAnimation.interpolator = LinearInterpolator()
        alphaAnimation.duration = 2000

        val scaleAnimation = ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        scaleAnimation.interpolator = CustomSpringInterpolator()
        scaleAnimation.duration = 2000

        val translateAnimation = TranslateAnimation(0f,0f,-1000f,0f)
        translateAnimation.interpolator = LinearInterpolator()
        translateAnimation.duration = 500

        val animationSet = AnimationSet(false)
        animationSet.addAnimation(rotateAnimation)
        animationSet.addAnimation(alphaAnimation)
        animationSet.addAnimation(scaleAnimation)
        animationSet.addAnimation(translateAnimation)

        val imageView = findViewById<ImageView>(R.id.app_iv)
        imageView.startAnimation(animationSet)
    }
}
