package dm.fer.hr.frontend;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.login.LoginManager;

import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.adapters.ViewPagerAdapter;
import dm.fer.hr.frontend.dialogs.MovieDetailsDialog;
import dm.fer.hr.frontend.dialogs.PickMoviesDialog;
import dm.fer.hr.frontend.fragments.AccountFragment;
import dm.fer.hr.frontend.fragments.NavigationTabFragment;
import dm.fer.hr.frontend.fragments.VeritcalListHorizontalScrollFragment;
import dm.fer.hr.frontend.fragments.pagetransformers.ZoomOutPageTransformer;
import dm.fer.hr.frontend.utils.SharedObjects;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.UserModel;

public class TabbedActivity extends AppCompatActivity {

    private BottomNavigationView navigation;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        initView();
    }

    private void initView() {
        initNavigation();
        initViewPager();
    }

    private void initNavigation() {
        navigation = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationView.OnNavigationItemSelectedListener navigationListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_popular:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.navigation_recommended:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.navigation_account:
                        viewPager.setCurrentItem(2);
                        return true;
                }
                return false;
            }
        };

        navigation.setOnNavigationItemSelectedListener(navigationListener);
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        pagerAdapter.addFragment(new VeritcalListHorizontalScrollFragment(), "1");
        NavigationTabFragment navigationTabFragment = new NavigationTabFragment();
        pagerAdapter.addFragment(navigationTabFragment, "2");
        pagerAdapter.addFragment(new AccountFragment(), "3");

        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        viewPager.setOnPageChangeListener( new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                navigation.setSelectedItemId(navigation.getMenu().getItem(i).getItemId());
                navigationTabFragment.refresh();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        viewPager.setOffscreenPageLimit(pagerAdapter.getCount());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LoginManager.getInstance().logOut();
    }
}
