package dm.fer.hr.frontend.adapters;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.dialogs.MovieDetailsDialog;
import dm.fer.hr.frontend.utils.LoaderUtil;
import dm.fer.hr.frontend.utils.SharedObjects;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.ApiException;
import io.swagger.client.api.MovieApi;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.UserModel;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    private List<MovieModel> movies;
    //cache for movies icon
    private HashMap<String, Bitmap> moviePosters;
    private SharedObjects<UserModel> sharedObjects;
    private FragmentActivity context;

    public MovieListAdapter(List<MovieModel> result, FragmentActivity context) {
        this.movies = result;
        this.moviePosters = new HashMap<>();
        this.sharedObjects = new SharedObjects<>(context);
        this.context = context;
        loadMovieIcons(movies);
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_movie_in_list, viewGroup, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int i) {
        View view = movieViewHolder.movieInListView;
        MovieModel movie = movies.get(i);
        TextView title = view.findViewById(R.id.title);
        TextView plot = view.findViewById(R.id.plot);
        title.setText(movie.getTitle());
        plot.setText(movie.getPlot());

        TextView rating1 = view.findViewById(R.id.rating_1_text);
        TextView rating2 = view.findViewById(R.id.rating_2_text);
        TextView rating3 = view.findViewById(R.id.rating_3_text);
        TextView rating4 = view.findViewById(R.id.rating_4_text);
        rating1.setText(movie.getMetacriticRating());
        rating2.setText(String.valueOf(movie.getTmdbRating()));
        rating3.setText(movie.getRottenTomatoesRating());
        rating4.setText(String.valueOf(movie.getImdbRating()));

        ImageView imageView = view.findViewById(R.id.image);
        if (moviePosters.keySet().contains(movie.getTmdbId() + movie.getId())
                && moviePosters.get(movie.getTmdbId() + movie.getId()) != null) {
            imageView.setImageBitmap(moviePosters.get(movie.getTmdbId() + movie.getId()));
        } else {
            imageView.setImageDrawable(view.getContext().getDrawable(R.mipmap.ic_launcher));
        }

        UserModel user = sharedObjects.retrive(Constants.user, UserModel.class);
        ImageView star = view.findViewById(R.id.favourite);
        if (user.getFavouriteMovies() != null && user.getFavouriteMovies().contains(movie)) {
            star.setImageDrawable(view.getContext().getDrawable(R.mipmap.ic_star));
        } else {
            star.setImageDrawable(view.getContext().getDrawable(R.mipmap.ic_star_hollow));
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        View movieInListView;
        MovieApi movieApi;

        MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            this.movieInListView = itemView;
            movieInListView.setOnClickListener(this);
            itemView.findViewById(R.id.favourite).setOnClickListener(this);
            movieApi = new MovieApi();
        }

        @Override
        public void onClick(View v) {
            MovieModel movie = movies.get(getAdapterPosition());

            if (v.getId() == R.id.favourite) {
                ImageView star = v.findViewById(R.id.favourite);

                UserModel user = sharedObjects.retrive(Constants.user, UserModel.class);
                if (user.getFavouriteMovies() == null) {
                    user.setFavouriteMovies(new ArrayList<>());
                }

                try {
                    if (user.getFavouriteMovies().contains(movie)) {
                        movieApi.apiFavouritesUserIdDeleteAsync(user.getId(), movie, new SimpleCallback<List<MovieModel>>() {
                            @Override
                            public void onSuccess(List<MovieModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                                super.onSuccess(result, statusCode, responseHeaders);
                                Drawable starDrawable = v.getContext().getDrawable(R.mipmap.ic_star_hollow);
                                star.setImageDrawable(starDrawable);
                                if (result != null) {
                                    user.setFavouriteMovies(result);
                                }
                                sharedObjects.put(Constants.user, user);
                                context.runOnUiThread(MovieListAdapter.this::notifyDataSetChanged);
                            }
                        });
                    } else {
                        movieApi.apiFavouritesUserIdPostAsync(user.getId(), movie, new SimpleCallback<List<MovieModel>>() {
                            @Override
                            public void onSuccess(List<MovieModel> result, int statusCode, Map<String, List<String>> responseHeaders) {
                                super.onSuccess(result, statusCode, responseHeaders);
                                Drawable starDrawable = v.getContext().getDrawable(R.mipmap.ic_star);
                                star.setImageDrawable(starDrawable);
                                if (result != null) {
                                    user.setFavouriteMovies(result);
                                }
                                sharedObjects.put(Constants.user, user);
                                context.runOnUiThread(MovieListAdapter.this::notifyDataSetChanged);
                            }
                        });
                    }
                } catch (ApiException e) {
                    e.printStackTrace();
                }
            } else {
                MovieDetailsDialog dialog = new MovieDetailsDialog();
                dialog.setMovie(movie);
                dialog.show(context.getSupportFragmentManager(), "MovieDetailsDialog");
            }
        }
    }

    private void loadMovieIcons(List<MovieModel> movies) {
        LoaderUtil loader = new LoaderUtil();
        loader.loadPosterImagesFromUrls(movies, new SimpleCallback<HashMap<String, Bitmap>>() {
            @Override
            public void onSuccess(HashMap<String, Bitmap> result, int statusCode, Map<String, List<String>> responseHeaders) {
                super.onSuccess(result, statusCode, responseHeaders);
                moviePosters.putAll(result);
                notifyDataSetChanged();
            }
        });
    }
}
