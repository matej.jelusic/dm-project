package dm.fer.hr.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import dm.fer.hr.frontend.Constants;
import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.adapters.MovieListAdapter;
import dm.fer.hr.frontend.utils.SharedObjects;
import io.swagger.client.model.MovieModel;
import io.swagger.client.model.UserModel;

public class WatchlistMovieListFragment extends Fragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        refresh();
    }

    private void fillData(View view, List<MovieModel> result) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        MovieListAdapter movieListAdapter = new MovieListAdapter(result, getActivity());
        recyclerView.setAdapter(movieListAdapter);
    }

    public void refresh() {
        super.onResume();
        if (view != null) {
            SharedObjects<UserModel> sharedObjects = new SharedObjects<>(view.getContext());
            UserModel user = sharedObjects.retrive(Constants.user, UserModel.class);
            if (user.getFavouriteMovies() == null) {
                user.setFavouriteMovies(new ArrayList<>());
            }
            fillData(view, user.getFavouriteMovies());
        }
    }
}
