package dm.fer.hr.frontend.adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.dialogs.MovieDetailsDialog;
import dm.fer.hr.frontend.utils.LoaderUtil;
import dm.fer.hr.frontend.utils.SimpleCallback;
import io.swagger.client.model.MovieModel;

public class MovieIconAdapter extends RecyclerView.Adapter<MovieIconAdapter.MovieIconViewHolder> {

    private FragmentActivity activity;
    private List<MovieModel> movies;
    //cache for movies icon
    HashMap<String, Bitmap> movieIcons;

    public MovieIconAdapter(List<MovieModel> movies) {
        this.movies = movies;
        this.movieIcons = new HashMap<>();
        loadMovieIcons(movies);
    }

    public int getMovieLayoutResId() {
        return R.layout.layout_movie_icon;
    }

    @NonNull
    @Override
    public MovieIconViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(getMovieLayoutResId(), viewGroup, false);
        return new MovieIconViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieIconViewHolder movieIconViewHolder, int i) {
        View view = movieIconViewHolder.movieIconView;
        MovieModel movie = movies.get(i);
        TextView textView = view.findViewById(R.id.text);
        textView.setText(movie.getTitle());
        //TODO use only movie.getId(), correct in LoaderUtil also
        ImageView imageView = view.findViewById(R.id.image);
        if (movieIcons.keySet().contains(movie.getTmdbId() + movie.getId())
                && movieIcons.get(movie.getTmdbId() + movie.getId()) != null) {
            imageView.setImageBitmap(movieIcons.get(movie.getTmdbId() + movie.getId()));
        } else {
            imageView.setImageDrawable(view.getResources().getDrawable(R.mipmap.ic_launcher));
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setContext(FragmentActivity activity) {
        this.activity = activity;
    }

    class MovieIconViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View movieIconView;

        MovieIconViewHolder(@NonNull View itemView) {
            super(itemView);
            this.movieIconView = itemView;
            this.movieIconView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            MovieDetailsDialog movieDetailsDialog = new MovieDetailsDialog();
            movieDetailsDialog.setMovie(movies.get(getAdapterPosition()));
            if (activity != null) {
                movieDetailsDialog.show(activity.getSupportFragmentManager(), "MovieIconAdapter");
            }
        }
    }

    private void loadMovieIcons(List<MovieModel> movies) {
        LoaderUtil loader = new LoaderUtil();
        loader.loadCoverImagesFromUrls(movies, new SimpleCallback<HashMap<String, Bitmap>>() {
            @Override
            public void onSuccess(HashMap<String, Bitmap> result, int statusCode, Map<String, List<String>> responseHeaders) {
                super.onSuccess(result, statusCode, responseHeaders);
                movieIcons.putAll(result);
                notifyDataSetChanged();
            }
        });
    }
}
