package dm.fer.hr.frontend.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import dm.fer.hr.frontend.R;
import dm.fer.hr.frontend.adapters.ViewPagerAdapter;
import dm.fer.hr.frontend.fragments.pagetransformers.DepthPageTransformer;
import info.hoang8f.android.segmented.SegmentedGroup;

public class NavigationTabFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener {

    ViewPagerAdapter adapter;
    ViewPager pager;
    SegmentedGroup segmentedGroup;
    RecommendedMovieListFragment recommendedMovieListFragment;
    WatchlistMovieListFragment watchlistMovieListFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_tab, container, false);
        segmentedGroup = view.findViewById(R.id.segmented);
        segmentedGroup.setOnCheckedChangeListener(this);
        segmentedGroup.setTintColor(getResources().getColor(R.color.colorPrimary));

        pager = view.findViewById(R.id.pager);
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());

        pager.setPageTransformer(true, new DepthPageTransformer());
        recommendedMovieListFragment = new RecommendedMovieListFragment();
        watchlistMovieListFragment = new WatchlistMovieListFragment();
        adapter.addFragment(recommendedMovieListFragment, "Recommended");
        adapter.addFragment(watchlistMovieListFragment, "Watchlist");
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(this);
        pager.addOnPageChangeListener(this);
        segmentedGroup.check(R.id.recommended);
        pager.setOffscreenPageLimit(adapter.getCount());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (R.id.recommended == checkedId) {
            pager.setCurrentItem(0, true);
            recommendedMovieListFragment.refresh();
        } else if (R.id.watchlist == checkedId) {
            pager.setCurrentItem(1, true);
            watchlistMovieListFragment.refresh();
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        if (i == 0) {
            segmentedGroup.check(R.id.recommended);
            recommendedMovieListFragment.refresh();
        } else if (i == 1) {
            segmentedGroup.check(R.id.watchlist);
            watchlistMovieListFragment.refresh();
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    public void refresh() {
        try {
            recommendedMovieListFragment.refresh();
            watchlistMovieListFragment.refresh();
        } catch (Exception e) {

        }
    }
}
