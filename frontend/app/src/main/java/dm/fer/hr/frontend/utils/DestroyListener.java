package dm.fer.hr.frontend.utils;

public interface DestroyListener {

    public void onDestroy();
}
